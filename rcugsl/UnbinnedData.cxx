// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Histogram.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:02:39 2007
    @brief   UnbinnedData class 
*/
#include "UnbinnedData.h"
#include <iostream>
#include <stdexcept>
// #include <gsl/gsl_statistics_double.h>

//____________________________________________________________________
RcuGsl::UnbinnedData::UnbinnedData(bool errors) 
  : fErrors(errors),
    fX(0), 
    fY(0), 
    fEx(0), 
    fEy(0),
    fFixedEx(0), 
    fFixedEy(0)
{}

//____________________________________________________________________
RcuGsl::UnbinnedData::UnbinnedData(size_t n, bool errors) 
  : fErrors(errors),
    fX(n), 
    fY(n), 
    fEx(errors ? n : 0), 
    fEy(errors ? n : 0),
    fFixedEx(0), 
    fFixedEy(0)
{}

//____________________________________________________________________
double
RcuGsl::UnbinnedData::X(size_t i) const 
{
  if (i >= Size()) throw std::domain_error("Invalid index");
  return fX[i];
}
//____________________________________________________________________
double
RcuGsl::UnbinnedData::Y(size_t i) const 
{
  if (i >= Size()) throw std::domain_error("Invalid index");
  return fY[i];
}
//____________________________________________________________________
double
RcuGsl::UnbinnedData::Ex(size_t i) const 
{
  if (!fErrors) return fFixedEx;
  if (i >= Size()) throw std::domain_error("Invalid index");
  return fEx[i];
}
//____________________________________________________________________
double
RcuGsl::UnbinnedData::Ey(size_t i) const
{
  if (!fErrors) return fFixedEy;
  if (i >= Size()) throw std::domain_error("Invalid index");
  return fEy[i];
}
//____________________________________________________________________
bool
RcuGsl::UnbinnedData::HasErrors() const 
{ 
  return fErrors || fFixedEx != 0 || fFixedEy != 0;
}
//____________________________________________________________________
void
RcuGsl::UnbinnedData::Set(size_t i, double x, double y) 
{
  if (fErrors) throw std::runtime_error("must specify errors");
  if (i >= Size()) throw std::runtime_error("index out of bounds");
  fX[i] = x;
  fY[i] = y;
}
//____________________________________________________________________
void
RcuGsl::UnbinnedData::Set(size_t i, double x, double y, double ex, double ey) 
{
  if (!fErrors || fEx.size() < fX.size() || fEy.size() < fX.size())
    throw std::runtime_error("cannot deal with errors");
  if (i >= Size()) throw std::runtime_error("index out of bounds");
  fX[i]  = x;
  fY[i]  = y;
  fEx[i] = ex;
  fEy[i] = ey;
}
//____________________________________________________________________
void
RcuGsl::UnbinnedData::Push(double x, double y) 
{
  if (fErrors) throw std::runtime_error("must specify errors");
  fX.push_back(x);
  fY.push_back(y);
}
//____________________________________________________________________
void
RcuGsl::UnbinnedData::Push(double x, double y, double ex, double ey) 
{
  if (!fErrors || fEx.size() < fX.size() || fEy.size() < fX.size())
    throw std::runtime_error("cannot deal with errors");
  fX.push_back(x);
  fY.push_back(y);
  fEx.push_back(ex);
  fEy.push_back(ey);
}
//____________________________________________________________________
void
RcuGsl::UnbinnedData::FixErrors(double ex, double ey) 
{
  fErrors  = false;
  fFixedEx = ex;
  fFixedEy = ey;
}

//____________________________________________________________________
//
// EOF
//
