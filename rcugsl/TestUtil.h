// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    TestUtil.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:08:07 2007
    @brief   Utilities for the tests 
    
*/
#ifndef RCUGSL_TestUtil
#define RCUGSL_TestUtil
#include <rcugsl/DataSet.h>
#include <rcugsl/Function.h>
#include <rcugsl/Fitter.h>
#include <rcugsl/Script.h>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <stdexcept>

/** Print for use with @b GNUPlot
    @ingroup util
    @param o output stream 
    @param g Function
    @param d Data set 
    @param chi2 On return, the 
    @f$\chi^2=\frac{1}{N}\sum_i^N\frac{(y_i-f(x_i))^2}{\sigma_i^2}@f$
    @param ndf On return, the number degrees of freedom. 
    @param print Whether to print */
void
print_gnuplot(std::ostream& o, const RcuGsl::Function& g, 
	      const RcuGsl::DataSet& d, double chi2, int ndf, bool print)  
{
  g.ToScript(o, RcuGsl::kGnuplotScript);
  if (print) 
    o << "set terminal postscript eps enhanced;\n" 
      << "set output 'graph.eps';" << std::endl;
  else 
    o << "set terminal x11 enhanced;\n";
  o << "set label '{/Symbol c}^2/{/Times-Roman NDF}: %8.4f'," << chi2 
    << ",'/%3.0f'," << ndf << ",' = %8.4f'," << chi2/ndf 
    << " at graph 0.7,0.8;" << std::endl;
  for (size_t i = 0; i < g.Size(); i++) 
    o << "set label '" << std::setw(10) << g.names()[i] 
      << ": %8.4f {\\261} ',"
      << g.Parameters()[i] << ",'%8.4f'," << g.Errors()[i]
      << " at graph 0.7," << 0.75-.05*i << ";" << std::endl;
  o << "plot f(x) title 'fit', '-' ";
  if (d.HasErrors()) o << "using 1:2:4 with errorbars ";
  o << "title 'data'\n";
  d.ToScript(o, RcuGsl::kGnuplotScript);
  o << "\ne;" << std::endl;
}

/** Print for use with @b ROOT
    @ingroup util
    @param o output stream 
    @param g Function
    @param d Data set */
void 
print_root(std::ostream& o, const RcuGsl::Function& g, 
	   const RcuGsl::DataSet& d)  
{
  o << "{\n";
  d.ToScript(o, RcuGsl::kRootScript);
  o << "  g->Draw(\"E\");\n";
  g.ToScript(o, RcuGsl::kRootScript);
  o << "  f->SetLineColor(2);\n" 
    << "  f->Draw(\"same\");\n"
    << "}" << std::endl;
}

/** Process command line arguments
    @ingroup util
    @param argc Number of arguments
    @param argv Vector of arguments
    @param verb Whether to be verbose
    @param root Whether to output to ROOT script
    @param plot Whether to output to GNUPlot scrpt
    @return @c true on success */
bool
process_cmdline(int argc, char** argv, bool& verb, bool& root, bool& plot)
{
  verb = root = plot = false;
  for (size_t i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'r': root = true; verb = false; break;
      case 'p': plot = true; verb = false; break;
      case 'v': root = plot = false; verb = true; break;
      default: 
	std::cout << "Usage: " << argv[0] << " [OPTIONS]\n\n"
		  << "Options:\n"
		  << "\t-r\tOutput ROOT script\n" 
		  << "\t-p\tOutput GNUPlot script\n" 
		  << "\t-v\tBe verbose\n" 
		  << std::endl;
	return false;
      }
    }
  }
  return true;
}

/** Do the actual fit
    @ingroup util
    @param f Fitter object
    @param g Function object
    @param h Data set object
    @param verb Whether to be verbose		   
    @param root Whether to output to ROOT script  
    @param plot Whether to output to GNUPlot scrpt
*/
void
fit(RcuGsl::Fitter& f, RcuGsl::Function& g, const RcuGsl::DataSet& h, 
    bool verb, bool root, bool plot)
{
  try {
    do {
      f.Step();
      if (verb) f.Print(0x3);
    } while (!f.TestDelta(1e-5, 1e-5) && f.Steps() < 300);
    
    if (!root && !plot) f.Print();  
    if (root) print_root(plot ? std::cerr : std::cout, g, h);
    if (plot) print_gnuplot(std::cout, g, h, 
			    f.ChiSquare(), f.DegreesOfFreedom(), false);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
  }
}

#endif
//
// EOF
//
