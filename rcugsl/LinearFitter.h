// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    LinearFitter.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:03:26 2007
    @brief   A linear fitter
*/
#ifndef RCUGSL_LinearFitter
#define RCUGSL_LinearFitter
#ifndef RCUGSL_Fitter
# include <rcugsl/Fitter.h>
#endif
#ifndef RCUGSL_Polynomial
# include <rcugsl/Polynomial.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __ALGORITHM__
# include <algorithm>
#endif
#ifndef __GSL_MULTIFIT_H__
# include <gsl/gsl_multifit.h>
#endif
#ifndef __GSL_VECTOR__
# include <gsl/gsl_vector.h>
#endif
#ifndef __GSL_MATRIX__
# include <gsl/gsl_matrix.h>
#endif

namespace RcuGsl
{
  
  //====================================================================
  /** @class LinearFitter LinearFitter.h <rcugsl/LinearFitter.h>
      @brief A fitter that will fit functions that are linear in the
      coefficents to data 
      @ingroup fit 
  */
  struct LinearFitter : public Fitter
  {
    /** Type of data arrays */
    typedef Fitter::Array_t Array_t;
  
    /** Constructor */
    LinearFitter();
    /** Constructor 
	@param f Function to fit to the DataSet @a d
	@param d Data set to fit function @a f to.  */
    LinearFitter(Polynomial& f, const DataSet& d);
    /** Destructor  */
    virtual ~LinearFitter();
    /** Set data to fit.
	@param f Function to fit to the DataSet @a d
	@param d Data set to fit function @a f to.
    	@param bugfix Fix for a bug in GSL  */
    void Set(Function& f, const DataSet& d, bool bugfix=true);
    /** Do one step of the iteration.  
	@throw std::runtime_error In case of progblems  */
    void Step();
    /** Check convergence. 
	@param epsabs not used
	@param epsrel not used
	@return always @c true */
    bool TestDelta(double epsabs, double epsrel) const { return true; }
    /** Check convergence.  The test is true if 
	@param epsabs not used
	@return always @c true */
    bool TestGradient(double epsabs) const { return true; }
    /** Get the covariance matrix.  Note, that the passed vector must be
	@f$ P\times P@f$ where @f$ P@f$ is the number of parameters of
	the function. 
	@param c On return, contains the covariance matrix, sorted in
	natural order (that is the column number varies most fast) */
    void Covariance(std::vector<double>& c) const;
    /** Get the gradient.  It's a vector with the same number of
	elements as parameters in the fitted function. 
	@param g On return, the null vector. */
    void Gradient(Array_t& g) const;
    /** Get the parameter values */
    void Values(Array_t& p) const;
    /** Get the @f$ \chi^2@f$ of the fit */
    double ChiSquare() const { return fChisq; }
    /** Print some  information */ 
    void Print(unsigned int mask=0) const;
    protected:
    /** Workspace */
    gsl_multifit_linear_workspace* fWorkspace;
    /** Predicator values */
    gsl_matrix* fX;
    /** Data values */ 
    gsl_vector* fY;
    /** Data weights */ 
    gsl_vector* fW;
    /** Found coefficents */ 
    gsl_vector* fC;
    /** The chi square */
    double fChisq;
  };
}

#endif
//
// EOF
//
