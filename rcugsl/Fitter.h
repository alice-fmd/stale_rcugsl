// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Fitter.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:01:06 2007
    @brief   Base class for fitters 
*/
#ifndef RCUGSL_Fitter
#define RCUGSL_Fitter
#ifndef RCUGSL_DataSet
# include <rcugsl/DataSet.h>
#endif
#ifndef RCUGSL_Function
# include <rcugsl/Function.h>
#endif
#ifndef __GSL_VECTOR_H__
# include <gsl/gsl_vector.h>
#endif
#ifndef __GSL_MATRIX_H__
# include <gsl/gsl_matrix.h>
#endif

namespace RcuGsl
{
  
  /** @defgroup fit Fitters 
      Classes in this module represents fitters */

  /** @class Fitter Fitter.h <rcugsl/Fitter.h>
      @brief Base class for fitters 
      @ingroup fit 

      A fitter is applied to a function and a data set.  The data set
      can be binned or unbinned.  

      For example, we can fill up a
      histogram with samples taken from a Guassian distribution 
      @code 
      RcuGsl::Histogram hist(100, -3, 3);
      double sigma = 1;
      for (size_t i = 0; i < 1000; i++) {
        // Generate Gaussian deviate 
        double x, y, r2;
	do { 
          // Pick uniformly a point in the square [-1,1]x[-1,1]
          x  = 2 * double(rand()) / RAND_MAX - 1;
	  y  = 2 * double(rand()) / RAND_MAX - 1;
          r2 = x * x + y * y;
        } while (r2 > 1 || r2 == 0); // In unit circle?
	// Box-Muller transform 
	x = sigma * y * sqrt(-2 * log(r2) / r2);
	hist.Fill(x);
      }
      @endcode 

      We can now define a fit function, and set the inital guess of
      the parameters.  We use the predefined function Gaus, and set
      the initial guess of the parameters to be the maximum, mean, and
      spread of the histogram. 
      @code 
      RcuGsl::Gaus gaus(hist.MaxContent(), hist.Mean(), hist.Sigma());
      @endcode 

      We will now fit the function to the data.  We use a non-linear
      fitter, as the fit function isn't linear in it's parameters. 
      @code 
      RcuGsl::NonLinearFitter fitter(gaus, hist);
      @endcode 
      
      Now we let the fitter iterate through to find the best function
      parameters.  Note, since Step may throw an exception on errors,
      we put all this into a @c try @c catch block 
      @code 
      try {
        do {
	  fitter.Step();
	} while (fitter.TestDelta() && fitter.Steps() < 300);
      @endcode 
      Note, that to will also stop the fitting if the fitter didn't
      finish in 300 steps.  
      
      Once the fitter is done, then the fit function will have the
      best parameters values set, along with the errors on them.  The
      @f$\chi^2@f$ of the fit can be retrieved from the fitting
      object.  We can print all this to standard output using the
      Print member function of the fitter 
      @code 
        fitter.Print();
      @endcode 
      
      Finally, we need to catch exceptions if they occured in the Step
      member function 
      @code 
      }
      catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
      }
      @endcode 
  */
  struct Fitter 
  {
    typedef std::vector<double> Array_t;
    /** Constructor */ 
    Fitter();
    
    /** Constructor 
	@param f Function to fit to the data set @a d
	@param d Data set to fit function @a f to.  */
    Fitter(Function& f, const DataSet& d);
    /** Destructor  */
    virtual ~Fitter();
    /** Set the fitter 
	@param f Function to fit to the data set @a d
	@param d Data set to fit function @a f to.  
    	@param bugfix Fix for a bug in GSL */
    virtual void Set(Function& f, const DataSet& d, bool bugfix=true);
    /** Do one step of the iteration.  
	@throw std::runtime_error In case of progblems  */
    virtual void Step() = 0;
    /** Check convergence.  The test is true if 
	@f[ 
	|dx_i| < \epsilon_{abs} + \epsilon_{rel} |x_i|
	@f]
	where @f$ dx_i@f$ is the last step size of the parameter @f$
	x_i@f$ of the fitted function. 
	@param epsabs @f$ \epsilon_{abs}@f$
	@param epsrel @f$ \epsilon_{rel}@f$
	@return @c true if the above condition is met for all @f$ i@f$ */
    virtual bool TestDelta(double epsabs=1e-5, 
			   double epsrel=1e-5) const = 0;
    /** Check convergence.  The test is true if 
	@f[ 
	\sum_i |g_i| < epsabs
	@f]
	where @f$ g_i@f$ is the gradient of the parameter @f$ x_i@f$ of
	the fitted function.  
	@param epsabs @f$ \epsilon_{abs}@f$
	@return @c true if the above condition is met for all @f$ i@f$ */
    virtual bool TestGradient(double epsabs=1e-3) const = 0;
    /** Get the covariance matrix.  Note, that the passed vector must be
	@f$ P\times P@f$ where @f$ P@f$ is the number of parameters of
	the function. 
	@param c On return, contains the covariance matrix, sorted in
	natural order (that is the column number varies most fast) */
    virtual void Covariance(std::vector<double>& c) const = 0;
    /** Get the gradient.  It's a vector with the same number of
	elements as parameters in the fitted function. 
	@param g On return, contains the gradient. */
    virtual void Gradient(Array_t& g) const = 0;
    /** Get the parameter errors */ 
    virtual void Errors(Array_t& e) const;
    /** Get the parameter Values */
    virtual void Values(Array_t& p) const = 0;
    /** Get the @f$ \chi^2@f$ of the fit */
    virtual double ChiSquare() const = 0;
    /** Get the number degrees of freedom */ 
    virtual size_t DegreesOfFreedom() const { return Samples() - Size(); }
    /** The number of steps */ 
    virtual size_t Steps() const { return fStep; }
    /** Print some  information */ 
    virtual void Print(unsigned int mask=0) const;
    /** Get the number of parameters */ 
    virtual size_t Size() const;
    /** Get the number of data points */ 
    virtual size_t Samples() const;

    /** @{ 
	@name Functions called by function interface */
    /** Evaluate the function at all points of the data set */
    int Eval(const gsl_vector* x, gsl_vector* f);
    /** Evaluate the function at all points of the data set */
    int Eval(const gsl_vector* x, gsl_matrix* j);
    /** Evaluate the function at all points of the DataSet */
    int Eval(const gsl_vector* x, gsl_vector* f, gsl_matrix* j);
    /** @} */
  protected:
    /** Store the result of the fit in the function object */ 
    virtual void Store() const;
    /** Function to fit to the data set */
    mutable Function*  fF;
    /** Data set to fit the function to */
    const DataSet* fD;
    /** Covariance matrix */
    mutable gsl_matrix* fCovar;
    /** Gradient */
    mutable gsl_vector* fGradient;
    /** Vector of parameters */ 
    Array_t fP;
    /** Cache of jacobian */ 
    Array_t fJ;
    /** Step number */ 
    size_t fStep;
  };
}

#endif
//
// EOF
//


  
