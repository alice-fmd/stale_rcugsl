// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    NonLinearFitter.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:04:35 2007
    @brief   A non-linear fitter
    
*/
#ifndef RCUGSL_NonLinearFitter
#define RCUGSL_NonLinearFitter
#ifndef RCUGSL_Fitter
# include <rcugsl/Fitter.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __ALGORITHM__
# include <algorithm>
#endif
#ifndef __GSL_MULTIFIT_NLIN_H__
# include <gsl/gsl_multifit_nlin.h>
#endif

namespace RcuGsl
{
  
  //====================================================================
  /** @class NonLinearFitter NonLinearFitter.h <rcugsl/NonLinearFitter.h>
      @brief A fitter that fits arbitrary functions to data 
      @ingroup fit 
  */
  struct NonLinearFitter : public Fitter
  {
    /** Type of data arrays */
    typedef Fitter::Array_t Array_t;
  
    /** Constructor */
    NonLinearFitter();
    /** Constructor 
	@param f Function to fit to the DataSet @a d
	@param d Data set to fit function @a f to.  */
    NonLinearFitter(Function& f, const DataSet& d);
    /** Destructor  */
    virtual ~NonLinearFitter();
    /** Set data to fit.
	@param f Function to fit to the DataSet @a d
	@param d Data set to fit function @a f to.  
	@param bugfix Fix for a bug in GSL */
    void Set(Function& f, const DataSet& d, bool bugfix=true);
    /** Do one step of the iteration.  
	@throw std::runtime_error In case of progblems  */
    void Step();
    /** Check convergence.  The test is true if 
	@f[ 
	|dx_i| < \epsilon_{abs} + \epsilon_{rel} |x_i|
	@f]
	where @f$ dx_i@f$ is the last step size of the parameter @f$
	x_i@f$ of the fitted function. 
	@param epsabs @f$ \epsilon_{abs}@f$
	@param epsrel @f$ \epsilon_{rel}@f$
	@return @c true if the above condition is met for all @f$ i@f$
    */
    bool TestDelta(double epsabs, double epsrel) const;
    /** Check convergence.  The test is true if 
	@f[ 
	\sum_i |g_i| < epsabs
	@f]
	where @f$ g_i@f$ is the gradient of the parameter @f$ x_i@f$ of
	the fitted function.  
	@param epsabs @f$ \epsilon_{abs}@f$
	@return @c true if the above condition is met for all @f$ i@f$
    */
    bool TestGradient(double epsabs) const;
    /** Get the covariance matrix.  Note, that the passed vector must be
	@f$ P\times P@f$ where @f$ P@f$ is the number of parameters of
	the function. 
	@param c On return, contains the covariance matrix, sorted in
	natural order (that is the column number varies most fast) */
    void Covariance(std::vector<double>& c) const;
    /** Get the gradient.  It's a vector with the same number of
	elements as parameters in the fitted function. 
	@param g On return, contains the gradient. */
    void Gradient(Array_t& g) const;
    /** Get the parameter values */
    void Values(Array_t& p) const;
    /** Get the @f$ \chi^2@f$ of the fit */
    double ChiSquare() const;
    /** Print some  information */ 
    void Print(unsigned int mask=0) const;
    protected:
    /** Fitter type */
    const gsl_multifit_fdfsolver_type* fType;
    /** The solver */
    gsl_multifit_fdfsolver* fSolver;
    /** The function to use */ 
    gsl_multifit_function_fdf fFDF;
    /** Current values */ 
    gsl_vector_view fCur;
  };
}

#endif
//
// EOF
//
