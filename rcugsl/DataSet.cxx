// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    DataSet.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:00:39 2007
    @brief   Base clas for data sets 
*/
#include "DataSet.h"
#include <iostream>
#include <iomanip>

void 
RcuGsl::DataSet::ToScript(std::ostream& o, Script_t t) const
{
  switch (t) {
  case kRootScript:
    o  << "  TGraph* g = new " 
       << (HasErrors() ? "TGraphErrors" : "TGraph") 
       << "(" << Size() << ");" << std::endl;
    for (size_t i = 0; i < Size(); i++) {
      o << "  g->SetPoint(" << i << "," << X(i) << "," << Y(i) 
	<< ");" << std::endl;
      if (HasErrors()) 
	o << "  g->SetPointErrors(" << i << "," << Ex(i) << "," 
	  << Ey(i) << ");" << std::endl;
    }
    break;
  case kGnuplotScript:
  case kRawScript:
    for (size_t i = 0; i < Size(); i++) {
      o << X(i) << "\t" << Y(i);
      if (HasErrors()) o << "\t" << Ex(i) << "\t" << Ey(i);
      o << std::endl;
    }
    break;
  }
}

//
// EOF
//
