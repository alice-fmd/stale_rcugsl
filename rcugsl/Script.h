/** @file    Script.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:06:36 2007
    @brief   Types of scripts 
    
*/
#ifndef RCUGSL_script
#define RCUGSL_script

namespace  RcuGsl
{
  
  /** @defgroup util Utilities 
      This module contains some utilities */
  /** Types of scripts 
      @ingroup basic
  */
  enum Script_t {
    /** ROOT script */
    kRootScript, 
    /** GNUPlot script */
    kGnuplotScript, 
    /** Raw data */
    kRawScript
  };
}

#endif
