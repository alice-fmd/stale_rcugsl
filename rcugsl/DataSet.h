// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    DataSet.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:00:19 2007
    @brief   Base class for data sets 
*/
#ifndef RCUGSL_DataSet
#define RCUGSL_DataSet
#ifndef RCUGSL_script
# include <rcugsl/Script.h>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif

namespace RcuGsl
{
  
  /** @defgroup data Data sets 
      This module contains classes for storing data, like a histogram or
      unbinned data sets */

  /** @class DataSet DataSet.h <rcugsl/DataSet.h>
      @brief Base class for all sorts of data sets 
      @ingroup data 
  */
  struct DataSet 
  {
    /** constructor */
    DataSet() {}
    /** Destructor */
    virtual ~DataSet() {}
    /** @return Number of points in the data set  */
    virtual size_t Size() const = 0;
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    virtual double X(size_t i) const = 0;
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    virtual double Y(size_t i) const = 0;
    /** @param i Entry number 
	@return  The error @f$ \Delta x@f$ at point @a i */
    virtual double Ex(size_t i) const { return 0; }
    /** @param i Entry number 
	@return  The error @f$ \Delta y@f$ at point @a i */
    virtual double Ey(size_t i) const { return 0; }
    /** Print data set to standard out 
	@param t Type of script to write 
	@param o Output stream */
    virtual void ToScript(std::ostream& o, Script_t t) const;
    /** @return true if we have errors */
    virtual bool HasErrors() const { return false; }
  };
}

#endif
//
// EOF
//


  
  
  
   
  
