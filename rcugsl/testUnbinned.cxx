#include <rcugsl/UnbinnedData.h>
#include <rcugsl/NonLinearFitter.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <rcugsl/TestUtil.h>
#include <sstream>

/** The function 
    @f[
    A e^{-\lambda x} + b
    @f]     
    @ingroup funcs
 */
struct func : public RcuGsl::Function
{
  /** Constructor 
      @param a The parameter @f$ A@f$
      @param lambda The parameter @f$ \lambda@f$
      @param b The parameter @f$ b@f$ */
  func(double a, double lambda, double b) 
    : RcuGsl::Function(3) 
  {
    Set(a, lambda, b);
    fNames[0] = "A";
    fNames[1] = "lambda";
    fNames[2] = "b";
  }
  /** Set parameters
      @param a The parameter @f$ A@f$
      @param lambda The parameter @f$ \lambda@f$
      @param b The parameter @f$ b@f$ */
  void Set(double a, double lambda, double b) 
  {
    fP[0] = a;
    fP[1] = lambda;
    fP[2] = b;
  }
  /** Evaluate function 
      @param x Function argument. 
      @return @f$ A e^{-\lambda x} + b@f$  */
  double operator()(double x) 
  {
    double a      = fP[0];
    double lambda = fP[1];
    double b      = fP[2];
    return  a * exp (-lambda * x) + b;
  }
  /** Get the Jacobian row corresponding to @f$ x@f$ 
      @f[ 
      J = \left(\begin{array}{lcl} 
        \frac{\partial f}{\partial A}       &=& e^{- \lambda x} \\ 
        \frac{\partial f}{\partial \lambda} &=& -x A e^{- \lambda x}\\
        \frac{\partial f}{\partial b}       &=& 1
	\end{array}\right)
      @f]
      
      @param x 
      @param j 
  */
  void Jacobian(double x, std::vector<double>& j)
  {
    double a      = fP[0];
    double lambda = fP[1];
    double b      = fP[2];
    double e      = exp(-lambda * x);
    j[0]          = e;
    j[1]          = -x * a * e;
    j[2]          = 1;
  }
  /** Get the function expression 
      @param t Script type 
      @return  Function expression */
  std::string ToString(RcuGsl::Script_t t) const
  {
    std::stringstream s;
    switch (t) {
    case RcuGsl::kRootScript: s << "\"[0]*exp(-[1]*x)+[2]\""; break;
    default:                  s << "A*exp(-lambda*x)+b";
    }
    return s.str();
  }
};
    
/** Test of non-linear fitter. The function 
    @f[
    A e^{-\lambda x} + b
    @f] 
    is fitted to data from the same function with parameters @f$ A=5,
    \lambda = 0.1, b=1@f$ smeared by a Gaussian and with fixed errors
    of @f$ \Delta y = 0.1@f$. 
    @ingroup tests
    @param argc # of arguments
    @param argv Vector of arguments
    @return 0 on success */
int 
main(int argc, char** argv)
{
  bool verb, root, plot;
  if (!process_cmdline(argc, argv, verb, root, plot)) return 0;
	
  try {
    func          g(5, .1,   1);
    RcuGsl::UnbinnedData d;
    d.FixErrors(0, .1);
    
    const gsl_rng_type* type = gsl_rng_default;
    gsl_rng*            r    = gsl_rng_alloc (type);
    
    for (size_t i = 0; i < 40; i++) {
      double t = i;
      d.Push(t, g(t) + gsl_ran_gaussian (r, .1));
    }
    g.Set(1.0, 0.0, 0.0);
    RcuGsl::NonLinearFitter f(g, d);
    fit(f, g, d, verb, root, plot);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
