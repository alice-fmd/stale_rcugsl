//
//
//   General C++ parser and lexer
//   Copyright (C) 2002  Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
//
/** @file   
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Examples. */


/** @page examples Examples 
    Here are the various examples.  
 */

/** @example testHisto.cxx  
    @par Test of non-linear fitter. The function 
    @f[
    A e^{-\frac{(x -\mu)^2}{2\sigma^2}}
    @f] 
    is fitted to data drawn from a Guassian distribution with @f$
    \mu=2, \sigma=1 @f$.  The data is histogrammed, and Poisson
    errors @f$ \Delta y_i = \sqrt{y_i}@f$ are assumed. 
/** @example testUnbinned.cxx
    @par Test of non-linear fitter. The function 
    @f[
    A e^{-\lambda x} + b
    @f] 
    is fitted to data from the same function with parameters @f$ A=5,
    \lambda = 0.1, b=1@f$ smeared by a Gaussian and with fixed errors
    of @f$ \Delta y = 0.1@f$.  */
/** @example testApprox.cxx
    @par Test of linear fitter. The function 
    @f[
    p_0 + p_1 * x + p_2 * x^2
    @f]
    is fitted to data from the function @f$ e^x@f$, with Guassian errors */
/** @example testLinear.cxx
    @par Test of linear fitter. A straight line is fitted to data from a
    straight line, smeared with a Guassian, and Poisson errors. */

#error Not for compilation
//
// EOF
//
