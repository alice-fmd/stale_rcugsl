dnl -*- mode: Autoconf -*- 
dnl
dnl  
dnl  ROOT generic rcugsl framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUGSL([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUGSL],
[
    # AC_REQUIRE([AC_RCUXX])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcugsl-prefix],
        [AC_HELP_STRING([--with-rcugsl-prefix],
		[Prefix where Rcugsl is installed])],
        rcugsl_prefix=$withval, rcugsl_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcugsl-url],
        [AC_HELP_STRING([--with-rcugsl-url],
		[Base URL where the Rcugsl dodumentation is installed])],
        rcugsl_url=$withval, rcugsl_url="")
    if test "x${RCUGSL_CONFIG+set}" != xset ; then 
        if test "x$rcugsl_prefix" != "x" ; then 
	    RCUGSL_CONFIG=$rcugsl_prefix/bin/rcugsl-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUGSL_CONFIG, rcugsl-config, no)
    rcugsl_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for Rcugsl version >= $rcugsl_min_version)

    # Check if we got the script
    rcugsl_found=no    
    if test "x$RCUGSL_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUGSL_CPPFLAGS=`$RCUGSL_CONFIG --cppflags`
       RCUGSL_INCLUDEDIR=`$RCUGSL_CONFIG --includedir`
       RCUGSL_LIBS=`$RCUGSL_CONFIG --libs`
       RCUGSL_LTLIBS=`$RCUGSL_CONFIG --ltlibs`
       RCUGSL_LIBDIR=`$RCUGSL_CONFIG --libdir`
       RCUGSL_LDFLAGS=`$RCUGSL_CONFIG --ldflags`
       RCUGSL_LTLDFLAGS=`$RCUGSL_CONFIG --ltldflags`
       RCUGSL_PREFIX=`$RCUGSL_CONFIG --prefix`

       # Check the version number is OK.
       rcugsl_version=`$RCUGSL_CONFIG -V` 
       rcugsl_vers=`echo $rcugsl_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcugsl_regu=`echo $rcugsl_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcugsl_vers -ge $rcugsl_regu ; then 
            rcugsl_found=yes
       fi
    fi
    AC_MSG_RESULT($rcugsl_found - is $rcugsl_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUGSL, [Whether we have rcugsl])

    if test "x$rcugsl_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUGSL_LIBDIR $RCUGSL_LIBS"
    	CPPFLAGS="$CPPFLAGS $RCUGSL_CPPFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcugsl_histogram_h=0
        AC_CHECK_HEADER([rcugsl/Histogram.h], 
	                [have_rcugsl_histogram_h=1])

        # Check the library. 
        have_librcugsl=no
        AC_MSG_CHECKING(for -lrcugsl)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcugsl/Histogram.h>],
                        [new Rcugsl::Histogram(100,0,10);])], 
                        [have_librcugsl=yes])
        AC_MSG_RESULT($have_librcugsl)

        if test $have_rcugsl_histogram_h -gt 0    && \
            test "x$have_librcugsl"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUGSL)
        else 
            rcugsl_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the Rcugsl documentation is installed)
    if test "x$rcugsl_url" = "x" && \
	test ! "x$RCUGSL_PREFIX" = "x" ; then 
       RCUGSL_URL=${RCUGSL_PREFIX}/share/doc/rcugsl/html
    else 
	RCUGSL_URL=$rcugsl_url
    fi	
    AC_MSG_RESULT($RCUGSL_URL)
   
    if test "x$rcugsl_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUGSL_URL)
    AC_SUBST(RCUGSL_PREFIX)
    AC_SUBST(RCUGSL_CPPFLAGS)
    AC_SUBST(RCUGSL_INCLUDEDIR)
    AC_SUBST(RCUGSL_LDFLAGS)
    AC_SUBST(RCUGSL_LIBDIR)
    AC_SUBST(RCUGSL_LIBS)
    AC_SUBST(RCUGSL_LTLIBS)
    AC_SUBST(RCUGSL_LTLDFLAGS)
])

dnl
dnl EOF
dnl 
