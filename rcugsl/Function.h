// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Function.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 01:59:53 2007
    @brief   Base class for functions 
*/
#ifndef RCUGSL_Function
#define RCUGSL_Function
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef __STRING__
# include <string>
#endif
#ifndef __IOSFWD__
# include <iosfwd>
#endif
#ifndef RCUGSL_Script
# include <rcugsl/Script.h>
#endif

namespace RcuGsl
{
  
  /** @defgroup funcs General and specific functions 
      Classes in this module represents functions. 
  */
  //====================================================================
  /** @class Function Function.h <rcugsl/Function.h>
      @brief Structure that represents a fit function 
      @ingroup funcs 
  */
  struct Function
  {
    /** Constructor 
	@param p number of parameters */ 
    Function(size_t p);
    /** Destructor  */
    virtual ~Function() {}
    /** Functor operator.  Evaluates the function at point @a x 
	@param x Where to evaluate the function 
	@return  */
    virtual double operator()(double x) = 0;
    /** Evaluate the jacobian of the function at point @a x
	@param x Where to evaluate the Jacobian
	@param j The Jacobian.  */
    virtual void Jacobian(double x, std::vector<double>& j) = 0;
    /** Set the parameters of the function.
	@param p Vector of parameters. */
    virtual void Set(const std::vector<double>& p);
    /** Set the parameters of the function.
	@param p Vector of parameters. 
	@param e Vector of error on parameters */
    virtual void Set(const std::vector<double>& p,
		     const std::vector<double>& e);
    /** Reset all parameters and errors to 0 */
    virtual void Reset();
    /** Get the number of parameters of the function */
    virtual size_t Size() const { return fP.size(); }
    /** Get reference to parameters */ 
    virtual std::vector<double>& Parameters() { return fP; }
    /** Get reference to parameters */ 
    virtual const std::vector<double>& Parameters() const { return fP; }
    /** Get reference to errors */ 
    virtual std::vector<double>& Errors() { return fE; }
    /** Get reference to errors */ 
    virtual const std::vector<double>& Errors() const { return fE; }
    /** Get the parameter names */
    virtual const std::vector<std::string>& names() const { return fNames; }
    /** Transform to string */
    virtual void ToScript(std::ostream& o, Script_t t) const;
    /** Print function on standard output */ 
    virtual void Print() const;
  protected:
    /** Get the function as a string */ 
    virtual std::string ToString(Script_t t) const = 0;
    /** Parameter values */
    std::vector<double> fP;
    /** Paramater errors */
    std::vector<double> fE;
    /** Parameter names */ 
    std::vector<std::string> fNames;
  };
}

#endif
//
// EOF
//
