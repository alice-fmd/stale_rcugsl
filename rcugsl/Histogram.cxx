// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Histogram.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:02:39 2007
    @brief   Histogram class 
*/
#include <rcugsl/Histogram.h>
#include <gsl/gsl_histogram.h>
#include <stdexcept>
#include <cmath>
#include <iostream>

//____________________________________________________________________
RcuGsl::Histogram::Histogram(const Histogram& other) 
{ 
  fH = gsl_histogram_clone(other.fH); 
}

//____________________________________________________________________
RcuGsl::Histogram& 
RcuGsl::Histogram::operator=(const Histogram& other) 
{
  Init(other.Bins(), other.Min(), other.Max());
  gsl_histogram_memcpy(fH, other.fH);
  return *this;
}

//____________________________________________________________________
RcuGsl::Histogram::~Histogram() 
{ 
  gsl_histogram_free(fH); 
}

//____________________________________________________________________
void 
RcuGsl::Histogram::Reset() 
{ 
  gsl_histogram_reset(fH); 
}

//____________________________________________________________________
size_t 
RcuGsl::Histogram::Bins() const 
{ 
  return gsl_histogram_bins(fH); 
}

//____________________________________________________________________
double 
RcuGsl::Histogram::Min() const 
{ 
  return gsl_histogram_min(fH); 
}

//____________________________________________________________________
double 
RcuGsl::Histogram::Max() const 
{ 
  return gsl_histogram_max(fH); 
}

//____________________________________________________________________
bool 
RcuGsl::Histogram::Fill(double x) 
{ 
  return gsl_histogram_increment(fH, x) == 0; 
}

//____________________________________________________________________
bool 
RcuGsl::Histogram::Fill(double x, double w) 
{
  return gsl_histogram_accumulate(fH, x, w) == 0; 
}


//____________________________________________________________________
double 
RcuGsl::Histogram::BinContent(size_t i) const
{
  if (i >= Bins()) throw std::domain_error("bin # out of range");
  return gsl_histogram_get(fH, i);
}

//____________________________________________________________________
double 
RcuGsl::Histogram::BinError(size_t i) const 
{
  double c = BinContent(i);
  return (c < 0 ? sqrt(-c) : sqrt(c));
}

//____________________________________________________________________
RcuGsl::Histogram::Range_t 
RcuGsl::Histogram::BinRange(size_t i) const 
{
  if (i >= Bins()) throw std::domain_error("bin # out of range");
  Range_t ret;
  gsl_histogram_get_range(fH, i, &(ret.first), &(ret.second));
  return ret;
}

//____________________________________________________________________
double 
RcuGsl::Histogram::BinCenter(size_t i) const
{
  Range_t r = BinRange(i);
  return (r.second - r.first) / 2 + r.first;
}

//____________________________________________________________________
int 
RcuGsl::Histogram::FindBin(double x) const
{
  size_t ret;
  if (gsl_histogram_find(fH, x, &ret) != 0) return -1;
  return ret;
}

//____________________________________________________________________
double
RcuGsl::Histogram::MaxContent() const 
{
  return gsl_histogram_max_val(fH); 
}

//____________________________________________________________________
size_t
RcuGsl::Histogram::MaxBin() const 
{
  return gsl_histogram_max_bin(fH); 
}

//____________________________________________________________________
double
RcuGsl::Histogram::MinContent() const 
{
  return gsl_histogram_min_val(fH); 
}

//____________________________________________________________________
size_t
RcuGsl::Histogram::MinBin() const 
{
  return gsl_histogram_min_bin(fH); 
}

//____________________________________________________________________
double
RcuGsl::Histogram::Mean() const 
{
  return gsl_histogram_mean(fH); 
}

//____________________________________________________________________
double
RcuGsl::Histogram::Sigma() const 
{
  return gsl_histogram_sigma(fH); 
}

//____________________________________________________________________
double
RcuGsl::Histogram::Sum() const 
{
  return gsl_histogram_sum(fH); 
}

//____________________________________________________________________
bool
RcuGsl::Histogram::IsCompatible(const Histogram& other) const 
  
{
  return gsl_histogram_equal_bins_p(fH, other.fH) == 1;
}

//____________________________________________________________________
RcuGsl::Histogram&
RcuGsl::Histogram::operator+=(const Histogram& other) 
{
  gsl_histogram_add(fH, other.fH);
  return *this;
}

//____________________________________________________________________
RcuGsl::Histogram&
RcuGsl::Histogram::operator-=(const Histogram& other) 
{
  gsl_histogram_sub(fH, other.fH);
  return *this;
}

//____________________________________________________________________
RcuGsl::Histogram&
RcuGsl::Histogram::operator*=(const Histogram& other) 
{
  gsl_histogram_mul(fH, other.fH);
  return *this;
}

//____________________________________________________________________
RcuGsl::Histogram&
RcuGsl::Histogram::operator/=(const Histogram& other) 
{
  gsl_histogram_div(fH, other.fH);
  return *this;
}

//____________________________________________________________________
RcuGsl::Histogram&
RcuGsl::Histogram::operator*=(double s) 
{
  gsl_histogram_scale(fH, s);
  return *this;
}

//____________________________________________________________________
RcuGsl::Histogram&
RcuGsl::Histogram::operator+=(double o) 
{
  gsl_histogram_scale(fH, o);
  return *this;
}

//____________________________________________________________________
void
RcuGsl::Histogram::ToScript(std::ostream& o, Script_t t) const
{
  switch (t) {
  case kRootScript: 
    o << "  TH1* g = new TH1D(\"h\", \"h\"," << Bins() << ","
      << Min() << "," << Max() << ");\n";
    for (size_t i = 0; i < Bins(); i++) 
      o << "  g->SetBinContent(" << i+1 << "," << BinContent(i) << ");\n"
	<< "  g->SetBinError("   << i+1 << "," << BinError(i) << ");" 
	<< std::endl;
    break;
  case kGnuplotScript:
  case kRawScript:
    for (size_t i = 0; i < Bins(); i++) 
      o << BinCenter(i)  << "\t" 
	<< BinContent(i) << "\t" 
	<< 0              << "\t"  
	<< BinError(i)   << std::endl;
    break;
  }
}
    
//____________________________________________________________________
void
RcuGsl::Histogram::Init(size_t n, double a, double b) 
{
  if (fH) gsl_histogram_free(fH);
  fH = gsl_histogram_alloc(n);
  gsl_histogram_set_ranges_uniform(fH, a, b);
}

//____________________________________________________________________
//
// EOF
//
