// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    UnbinnedData.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:08:33 2007
    @brief   Unbinned data storage
*/
#ifndef RCUGSL_UnbinnedData
#define RCUGSL_UnbinnedData
#ifndef RCUGSL_DataSet
# include <rcugsl/DataSet.h>
#endif
#ifndef __VECTOR__
# include <vector>
#endif

namespace RcuGsl
{
  
  /** @class UnbinnedData UnbinnedData.h <rcugsl/UnbinnedData.h>
      @brief A set of data 
      @ingroup data 
  */
  struct UnbinnedData : public DataSet
  {
    /** Constructor 
	@param errors Wheter to have individual errors  */
    UnbinnedData(bool errors);
    /** Constructor 
	@param n size of the data set 
	@param errors Wheter to have individual errors  */
    UnbinnedData(size_t n=0, bool errors=true);
    /** @return Number of points in the data set  */
    virtual size_t Size() const { return fX.size(); }
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    virtual double X(size_t i) const;
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    virtual double Y(size_t i) const;
    /** @param i Entry number 
	@return  The error @f$ \Delta x@f$ at point @a i */
    virtual double Ex(size_t i) const;
    /** @param i Entry number 
	@return  The error @f$ \Delta y@f$ at point @a i */
    virtual double Ey(size_t i) const;
    /** @return true if we have errors */
    virtual bool HasErrors() const;
    /** Set the @a i data point
	@param i Data point to set
	@param x X value 
	@param y Y value 
	@throw std::runtime_error if errors are requested, or if the
	index @a i is out of bounds  */
    void Set(size_t i, double x, double y);
    /** Set the @a i data point
	@param i Data point to set
	@param x X value 
	@param y Y value 
	@param ex Error on X
	@param ey Error on Y
	@throw std::runtime_error if errors are not requested, or if the 
	index @a i is out of bounds  */
    void Set(size_t i, double x, double y, double ex, double ey);
    /** Push a data point into the set.  The data is appended to the
	current set. 
	@param x X value 
	@param y Y value 
	@throw std::runtime_error if errors are requested */
    void Push(double x, double y);
    /** Push a data point into the set.  The data is appended to the
	current set. 
	@param x X value 
	@param y Y value 
	@param ex Error on X
	@param ey Error on Y
	@throw std::runtime_error if errors are not requested */
    void Push(double x, double y, double ex, double ey);
    /** Set fixed errors */ 
    void FixErrors(double ex, double ey);    
  protected: 
    /** Type of arrays */
    typedef std::vector<double> Array_t;
    /** Whether we want errors */
    bool    fErrors;
    /** Array of X values */
    Array_t fX;
    /** Array of Y values */
    Array_t fY;
    /** Array of errors on X values */
    Array_t fEx;
    /** Array of errors on Y values */
    Array_t fEy;
    /** Fixed X error */
    double  fFixedEx;
    /** Fixed Y error */
    double  fFixedEy;
  };
}

#endif
//
// EOF
//


