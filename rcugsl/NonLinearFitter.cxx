// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    NonLinearFitter.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:04:25 2007
    @brief   Non-linear fitter
*/
#include "NonLinearFitter.h"
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_permutation.h>

namespace 
{
  typedef struct {
    size_t iter;
    double xnorm;
    double fnorm;
    double delta;
    double par;
    gsl_matrix *r;
    gsl_vector *tau;
    gsl_vector *diag;
    gsl_vector *qtf;
    gsl_vector *newton;
    gsl_vector *gradient;
    gsl_vector *x_trial;
    gsl_vector *f_trial;
    gsl_vector *df;
    gsl_vector *sdiag;
    gsl_vector *rptdx;
    gsl_vector *w;
    gsl_vector *work1;
    gsl_permutation * perm;
  } lmder_state_t;
  void reset_lmder_state(void* state) 
  {
    lmder_state_t* s = (lmder_state_t*)state;
    if (!s) return;
    s->iter  = 0;
    s->xnorm = 0;
    s->fnorm = 0;
    s->delta = 0;
    s->par   = 0;
    if (s->r)        gsl_matrix_set_zero((s->r));
    if (s->tau)      gsl_vector_set_zero((s->tau));
    if (s->diag)     gsl_vector_set_zero((s->diag));
    if (s->newton)   gsl_vector_set_zero((s->newton));
    if (s->gradient) gsl_vector_set_zero((s->gradient));
    if (s->x_trial)  gsl_vector_set_zero((s->x_trial));
    if (s->f_trial)  gsl_vector_set_zero((s->f_trial));
    if (s->df)       gsl_vector_set_zero((s->df));
    if (s->sdiag)    gsl_vector_set_zero((s->sdiag));
    if (s->rptdx)    gsl_vector_set_zero((s->rptdx));
    if (s->w)        gsl_vector_set_zero((s->w));
    if (s->work1)    gsl_vector_set_zero((s->work1));
    if (s->perm)     gsl_permutation_init((s->perm));
  }

  //====================================================================
  int fit_f(const gsl_vector* x, void* param, gsl_vector* f)
  {
    RcuGsl::Fitter* fp = static_cast<RcuGsl::Fitter*>(param);
    if (!fp) return 1;
    return fp->Eval(x, f);
  }

  int fit_df(const gsl_vector* x, void* param, gsl_matrix* j)
  {
    RcuGsl::Fitter* fp = static_cast<RcuGsl::Fitter*>(param);
    if (!fp) return 1;
    return fp->Eval(x, j);
  }
  int fit_fdf(const gsl_vector* x, void* param, gsl_vector* f, gsl_matrix* j)
  {
    RcuGsl::Fitter* fp = static_cast<RcuGsl::Fitter*>(param);
    if (!fp) return 1;
    return fp->Eval(x, f, j);
  }
}

//____________________________________________________________________
RcuGsl::NonLinearFitter::NonLinearFitter() 
  : Fitter(),
    fType(gsl_multifit_fdfsolver_lmder), 
    fSolver(0)
{}

//____________________________________________________________________
RcuGsl::NonLinearFitter::NonLinearFitter(Function& f, const DataSet& h) 
  : Fitter(f, h),
    fType(gsl_multifit_fdfsolver_lmder), 
    fSolver(0)
{
  Set(f, h);
}

//____________________________________________________________________
RcuGsl::NonLinearFitter::~NonLinearFitter() 
{
  if (fSolver)   gsl_multifit_fdfsolver_free(fSolver);
}


//____________________________________________________________________
void
RcuGsl::NonLinearFitter::Set(Function& f, const DataSet& d, bool bugfix)
{
  Fitter::Set(f, d);
  fFDF.f       = fit_f;
  fFDF.df      = fit_df;
  fFDF.fdf     = fit_fdf;
  fFDF.p       = fF->Size();
  fFDF.n       = fD->Size();
  fFDF.params  = this;

  if (fSolver && fSolver->fdf && 
      (fSolver->fdf->n != fFDF.n || fSolver->fdf->p != fFDF.p)) {
     gsl_multifit_fdfsolver_free(fSolver);
     fSolver = 0;
  }
  if (!fSolver) 
    fSolver    = gsl_multifit_fdfsolver_alloc(fType, Samples(), Size());
  
  fCur         = gsl_vector_view_array(&(f.Parameters()[0]), Size());
  if (bugfix) {
    // gsl_vector_set_zero(fSolver->dx);
    // gsl_vector_set_zero(fSolver->f);
    // gsl_vector_set_zero(fSolver->x);
    // gsl_matrix_set_zero(fSolver->J);
    reset_lmder_state(fSolver->state);
  }
  gsl_multifit_fdfsolver_set(fSolver, &fFDF, &fCur.vector);
}


//____________________________________________________________________
void
RcuGsl::NonLinearFitter::Step() 
{
  int status = gsl_multifit_fdfsolver_iterate(fSolver);
  if (status) {
    std::stringstream s;
    s << "In step " << fStep << " of fit: " << gsl_strerror(status);
    throw std::runtime_error(s.str());
  }
  fStep++;
}

//____________________________________________________________________
bool
RcuGsl::NonLinearFitter::TestDelta(double epsabs, double epsrel) const
{
  if (!fSolver) return true;
  int ret = gsl_multifit_test_delta(fSolver->dx, 
				    fSolver->x, 
				    epsabs, 
				    epsrel);
  if (ret != GSL_CONTINUE) Store();
  return ret != GSL_CONTINUE;
}

//____________________________________________________________________
bool
RcuGsl::NonLinearFitter::TestGradient(double epsabs) const
{
  if (!fSolver) return true;
  Array_t g(fFDF.p);
  Gradient(g);
  int ret = gsl_multifit_test_gradient(fGradient, epsabs) != GSL_CONTINUE;
  if (ret != GSL_CONTINUE) Store();
  return ret == GSL_CONTINUE;
}
  
//____________________________________________________________________
void
RcuGsl::NonLinearFitter::Covariance(std::vector<double>& c) const
{
  if (!fSolver) return;
  if (!fCovar) fCovar = gsl_matrix_alloc(Size(), Size());
  gsl_multifit_covar(fSolver->J, 0., fCovar);
  for (size_t i = 0; i < Size(); i++) {
    for (size_t j = 0; j < Size(); j++) {
      c[i * Size() + j] = gsl_matrix_get(fCovar, i, j);
    }
  }    
}

//____________________________________________________________________
void
RcuGsl::NonLinearFitter::Gradient(Array_t& g) const 
{
  if (!fSolver) return;
  if (!fGradient) fGradient = gsl_vector_alloc(Size());
  gsl_multifit_gradient(fSolver->J, fSolver->f, fGradient);
  for (size_t i = 0; i < Size(); i++) 
    g[i] = gsl_vector_get(fGradient, i);
}

//____________________________________________________________________
void
RcuGsl::NonLinearFitter::Values(Array_t& p) const
{
  for (size_t i = 0; i < Size(); i++) p[i] = gsl_vector_get(fSolver->x, i);
}

/** Get the @f$ \chi^2@f$ of the fit */
//____________________________________________________________________
double
RcuGsl::NonLinearFitter::ChiSquare() const 
{ 
  if (!fSolver) return -1;
  double chi = gsl_blas_dnrm2(fSolver->f); 
  return pow(chi,2);
}


//____________________________________________________________________
void
RcuGsl::NonLinearFitter::Print(unsigned int mask) const 
{
  if (mask & 0x1 || mask & 0x2) 
    std::cout << "Step # " << fStep << "\t: " << ChiSquare() << std::endl;
  if (mask & 0x1 && fSolver) {
    std::cout << "  x={";
    for (size_t i = 0; i < Size(); i++) 
      std::cout << (i == 0 ? "" : ",") << gsl_vector_get(fSolver->x, i);
    std::cout << "}" << std::endl;
  }
  if (mask & 0x2 && fSolver) {
    std::cout << "  dx={";
    for (size_t i = 0; i < Size(); i++) 
      std::cout << (i == 0 ? "" : ",") << gsl_vector_get(fSolver->dx, i);
    std::cout << "}" << std::endl;
  }
  Fitter::Print(mask);
  
}

//
// EOF
//
