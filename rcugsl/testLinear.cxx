// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugsl/UnbinnedData.h>
#include <rcugsl/Polynomial.h>
#include <rcugsl/LinearFitter.h>
#include "TestUtil.h"
#include <stdexcept>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

/** Test of linear fitter. A straight line is fitted to data from a
    straight line, smeared with a Guassian, and Poisson errors. 
    @ingroup tests
    @param argc # of arguments
    @param argv Vector of arguments
    @return 0 on success */
int 
main(int argc, char** argv)
{
  bool verb, root, plot;
  if (!process_cmdline(argc, argv, verb, root, plot)) return 0;
	
  try {
    RcuGsl::UnbinnedData d(true);
    
    const gsl_rng_type* type = gsl_rng_default;
    gsl_rng*            r    = gsl_rng_alloc (type);
    
    size_t i = 0;
    for (unsigned adc = 0; adc < 255; adc += 30) {
      double x  = adc;
      double y  = 3 * adc + gsl_ran_gaussian (r, 5);
      double ey = gsl_ran_poisson (r, 20);
      d.Push(x, y, 0, ey);
    }
    
    RcuGsl::Polynomial p(0, 0);
    RcuGsl::LinearFitter f(p, d);
    fit(f, p, d, verb, root, plot);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}
