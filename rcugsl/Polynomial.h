// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Polynomial.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:05:16 2007
    @brief   A Polynomial function
    
*/
#ifndef RCUGSL_Polynomial
#define RCUGSL_Polynomial
#ifndef RCUGSL_Function
# include <rcugsl/Function.h>
#endif
#ifndef __CMATH__
# include <cmath>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif

namespace RcuGsl
{
  
  /** @class Polynomial Polynomial.h <rgugsl/Polynomial.h>
      @brief A class that represents Polynomial functions of the type 
      @f[ 
      \sum_i^N p_i x_i^i
      @f]
      @ingroup funcs
  */
  struct Polynomial : public Function 
  {
    /** Constructor of a 1st order Polynomial 
	@f[
	f(x) = p_0 + p_1 x
	@f]
	@param p0 
	@param p1 */
    Polynomial(double p0, double p1) 
      : Function(2) 
    {
      std::vector<double> c(2);
      c[0] = p0;
      c[1] = p1;
      Set(c);
    }
    /** Constructor of a 2nd order Polynomial 
	@f[
	f(x) = p_0 + p_1 x + p_2 x^2 
	@f]
	@param p0 
	@param p1 
	@param p2  */
    Polynomial(double p0, double p1, double p2) 
      : Function(3) 
    {
      std::vector<double> c(3);
      c[0] = p0;
      c[1] = p1;
      c[2] = p2;
      Set(c);
    }
    /** Constructor 
	@param c Vector of coefficents  */
    Polynomial(const std::vector<double>& c)
      : Function(c.size())
    {
      Set(c);
    }
    /** Destructor */
    virtual ~Polynomial() {}
    /** Evaluates the function
	@f[
	f: x\rightarrow \sum_i p_i x^ia
	@f]
	at the point @f$ x=@f$ @a x 
	@param x Where to evaluate the function
	@return @f$ f(x)@f$ */
    virtual double operator()(double x) 
    {
      double ret = fP[0];
      for (size_t i = 1; i < Size(); i++) ret += fP[i] * pow(x, i);
      return ret;
    }
    /** Return in @a j the Jacobian of the function. Since the function
	is linear in the coefficents, the only term that will contribute
	to the elements of Jacobian matrix is the term corresponding to
	the current column.   Hence, a term is given by 
	@f[ 
	J_{ij} = x_j^i
	@f]
	@param x where to evaluate the Jacobian 
	@param j On return, contains the row of the Jacobian */
    virtual void Jacobian(double x, std::vector<double>& j) 
    {
      j[0] = 0;
      for (size_t i = 1; i < Size(); i++) j[i] = pow(x, i);
    }
    /** Return function as a string */ 
    virtual std::string ToString(Script_t t) const 
    {
      std::stringstream s;
      switch (t) {
      case kRootScript: 
	s << "[0]";
	for (size_t i = 1; i < Size(); i++) 
	  s << "+[" << i << "] * pow(x," << i;
	break;
      default: 
	s << fNames[0];
	for (size_t i = 1; i < Size(); i++) 
	  s << "+" << fNames[i] << "*x**" << i;
	break;
      }
      return s.str();
    }
  };
}

#endif


      

  
      
      

