//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    LinearFitter.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:03:16 2007
    @brief   A linear fitter 
*/
#include "LinearFitter.h"
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multifit.h>

//____________________________________________________________________
RcuGsl::LinearFitter::LinearFitter() 
  : Fitter(),
    fWorkspace(0), 
    fX(0), 
    fY(0), 
    fW(0),
    fC(0),
    fChisq(-1)
{}

//____________________________________________________________________
RcuGsl::LinearFitter::LinearFitter(Polynomial& f, const DataSet& h) 
  : Fitter(f, h),
    fWorkspace(0), 
    fX(0), 
    fY(0), 
    fW(0),
    fC(0),
    fChisq(-1)
{
  Set(f, h);
}

//____________________________________________________________________
RcuGsl::LinearFitter::~LinearFitter() 
{
  if (fWorkspace)   gsl_multifit_linear_free(fWorkspace);
  if (fX)           gsl_matrix_free(fX);
  if (fY)           gsl_vector_free(fY);
  if (fC)           gsl_vector_free(fC);
  if (fW)           gsl_vector_free(fW);
}


//____________________________________________________________________
void
RcuGsl::LinearFitter::Set(Function& f, const DataSet& d, bool)
{
  Polynomial* pol = dynamic_cast<Polynomial*>(&f);
  if (!pol) 
    throw std::runtime_error("Function is not linear in the parameters");
  Fitter::Set(f, d);
  size_t p = fF->Size();
  size_t n = fD->Size();
  bool   e = fD->HasErrors();

  if (fWorkspace && (fWorkspace->n != n || fWorkspace->p != p)) {
    gsl_multifit_linear_free(fWorkspace);
    fWorkspace = 0;
  }

  if (fCovar && fCovar->size1 != p) {
    gsl_matrix_free(fCovar);
    fCovar = 0;
  }
  if (fC && fC->size != p) {
    gsl_vector_free(fC);
    fC = 0;
  }
  if (fY && fY->size != n) {
    gsl_vector_free(fY);
    fY = 0;
  }
  if (e && fW && fW->size != n) {
    gsl_vector_free(fW);
    fW = 0;
  }
  if (fX && (fX->size1 != n || fX->size2 != p)) {
    gsl_matrix_free(fX);
    fX = 0;
  }

  if (!fWorkspace) fWorkspace = gsl_multifit_linear_alloc(n, p);
  if (!fCovar)     fCovar     = gsl_matrix_alloc(p, p);
  if (!fX)         fX         = gsl_matrix_alloc(n, p);
  if (!fY)         fY         = gsl_vector_alloc(n);
  if (!fC)         fC         = gsl_vector_alloc(p);
  if (e && !fW)    fW         = gsl_vector_alloc(n);
  fChisq = -1;
}


//____________________________________________________________________
void
RcuGsl::LinearFitter::Step() 
{
  if (!fF) throw std::runtime_error("No fit function defined");
  if (!fD) throw std::runtime_error("No data to fit");

  if (fStep > 0) return;
  size_t p = fF->Size();
  size_t n = fD->Size();
  bool   e = fD->HasErrors();
  
  // Now, build up predicator matrix and so on 
  for (size_t i = 0; i < n; i++) {
    double x  = fD->X(i);
    double ey = fD->Ey(i);
    gsl_vector_set(fY, i, fD->Y(i));
    gsl_matrix_set(fX, i, 0, 1);
    for (size_t j = 1; j < p; j++) gsl_matrix_set(fX, i, j, pow(x, j));
    if (e) gsl_vector_set(fW, i, 1. / (ey * ey));
  }
  int status = 0;
  if (e) 
    status = gsl_multifit_wlinear (fX, fW, fY, fC, fCovar, &fChisq,fWorkspace);
  else   
    status = gsl_multifit_linear(fX, fY, fC, fCovar, &fChisq, fWorkspace);
  
  if (status) {
    std::stringstream s;
    s << "In step " << fStep << " of fit: " << gsl_strerror(status);
    throw std::runtime_error(s.str());
  }
  Store();
  fStep++;
}

//____________________________________________________________________
void
RcuGsl::LinearFitter::Covariance(std::vector<double>& c) const
{
  if (!fCovar) throw std::runtime_error("no fit performed");
  size_t p = Size();
  for (size_t i = 0; i < p; i++) {
    for (size_t j = 0; j < p; j++) {
      c[i * p + j] = gsl_matrix_get(fCovar, i, j);
    }
  }    
}

//____________________________________________________________________
void
RcuGsl::LinearFitter::Gradient(Array_t& g) const 
{
  for (size_t i = 0; i < Size(); i++) g[i] = 0;
}

//____________________________________________________________________
void
RcuGsl::LinearFitter::Values(Array_t& p) const
{
  for (size_t i = 0; i < Size(); i++) p[i] = gsl_vector_get(fC, i);
}

//____________________________________________________________________
void
RcuGsl::LinearFitter::Print(unsigned int mask) const 
{
  if (mask & 0x1 || mask & 0x2) 
    std::cout << "Step # " << fStep << "\t: " << ChiSquare() << std::endl;
  if (mask & 0x1) {
    std::cout << "  x={";
    for (size_t i = 0; i < Size(); i++) 
      std::cout << (i == 0 ? "" : ",") << gsl_vector_get(fC, i);
    std::cout << "}" << std::endl;
  }
  Fitter::Print(mask);  
}

//
// EOF
//
