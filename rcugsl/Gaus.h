// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Gaus.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:02:19 2007
    @brief   A Gaussian function 
    
*/
#ifndef RCUGSL_Gaus
#define RCUGSL_Gaus
#ifndef RCUGSL_Function
# include <rcugsl/Function.h>
#endif
#ifndef __CMATH__
# include <cmath>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif

namespace RcuGsl
{
  
  //====================================================================
  /** @class Gaus Gaus.h <rcugsl/Gaus.h> 
      @brief Structure that represents a Guassian function 
      @f[ 
      f: x\rightarrow a e^{-\frac{(x - \mu)^2}{2\sigma^2}}
      @f]
      @ingroup funcs
  */
  struct Gaus : public Function
  {
    /** Constructor 
	@param a Scale 
	@param mu Mean
	@param sigma Spread */
    Gaus(double a, double mu, double sigma) 
      : Function(3)
    {
      Set(a, mu, sigma);
      fNames[0] = "A";
      fNames[1] = "mu";
      fNames[2] = "sigma";
    }
    /** Set parameters
	@param a Scale 
	@param mu Mean
	@param sigma Spread */
    void Set(double a, double mu, double sigma) 
    { 
      fP[0] = a;
      fP[1] = mu;
      fP[2] = sigma;
    }
    /** Evaluates the function
	@f[
	f: x\rightarrow a e^{-\frac{(x - \mu)^2}{2\sigma^2}}
	@f]
	at the point @f$ x=@f$ @a x 
	@param x Where to evaluate the function
	@return @f$ f(x)@f$ */
    double operator()(double x) 
    {
      return fP[0] * ExpPart(x);
    }
  

    /** Return in @a j the Jacobian of the function 
	@f[
	J = \left(\begin{array}{lcl} 
        \frac{\partial f}{\partial a} &=& e^{- (x-\mu)^2 / \sigma^2} \\ 
        \frac{\partial f}{\partial \mu} &=&
	\frac{2 a (x - \mu)}{\sigma^2} e^{- (x-\mu)^2 / \sigma^2} \\ 
        \frac{\partial f}{\partial \sigma} &=& 
	\frac{2 a (x - \mu)^2}{\sigma^3} e^{- (x-\mu)^2 / \sigma^2} \\ 
	\end{array}\right)
	@f]
	evaluated at the point @f$ x@f$. 
	@param x Where to evaluate the Jacobian 
	@param j On return, contains the three Jacobian entries */
    void Jacobian(double x, std::vector<double>& j)
    {
      double a     = fP[0];
      double mu    = fP[1];
      double sigma = fP[2];
      double c  = (x - mu);
      double c2 = pow(c, 2);
      double ex = ExpPart(x);
      j[0]      = ex;
      j[1]      = a * c  * ex / pow(sigma, 2);
      j[2]      = a * c2 * ex / pow(sigma, 3);
    }
    /** Return function as a string */
    std::string ToString(Script_t t) const
    {
      std::stringstream s;
      switch (t) {
      case kRootScript: 
	s << "[0]*exp(-.5*pow((x-[1])/[2],2))";
	break;
      case kGnuplotScript: 
      case kRawScript: 
	s << fNames[0] << "*exp(-.5*((x-" << fNames[1] << ")/" 
	  << fNames[2] << ")**2)"; 
	break;
      }
    
      return s.str();
    }
  private:
    /** Helper function.  Computes 
	@f[
	e^{-\frac{(x - \mu)^2}{2\sigma^2}}
	@f]
	@param x Where to evaluate the expression. 
	@return Expression evaluated at @f$ x=@f$ @a x */
    double ExpPart(double x) 
    {
      double mu    = fP[1];
      double sigma = fP[2];
      return exp(- .5 * pow(x - mu, 2) / pow(sigma, 2));
    }
  };
}

#endif
//
// EOF
//
