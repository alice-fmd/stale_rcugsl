// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcugsl/Gaus.h>
#include <rcugsl/Histogram.h>
#include <rcugsl/NonLinearFitter.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <rcugsl/TestUtil.h>

/** @defgroup tests Tests 
    Tests of the stuff 
*/
/** Test of non-linear fitter. The function 
    @f[
    A e^{-\frac{(x -\mu)^2}{2\sigma^2}}
    @f] 
    is fitted to data drawn from a Guassian distribution with @f$
    \mu=2, \sigma=1 @f$.  The data is histogrammed, and Poisson
    errors @f$ \Delta y_i = \sqrt{y_i}@f$ are assumed. 
    @ingroup tests
    @param argc # of arguments
    @param argv Vector of arguments
    @return 0 on success */
int 
main(int argc, char** argv)
{
  bool verb, root, plot;
  if (!process_cmdline(argc, argv, verb, root, plot)) return 0;
	
  try {
    RcuGsl::Histogram h(100, -1, 5);
    
    const gsl_rng_type* type = gsl_rng_default;
    gsl_rng*            r    = gsl_rng_alloc (type);
    
    for (size_t i = 0; i < 10000; i++) 
      h.Fill(gsl_ran_gaussian (r, 1) + 2);
    
    
    RcuGsl::Gaus g(0.1, 0, .1);
    RcuGsl::NonLinearFitter f;
    f.Set(g, h);
    fit(f, g, h, verb, root, plot);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

//
// EOF
//
