#include <iostream>
#include <fstream>
#include <rcugsl/Histogram.h>
#include <rcugsl/NonLinearFitter.h>
#include <rcugsl/Gaus.h>
#include <vector>
#include <string>
#include <stdexcept>

int
main(int argc, char** argv)
{
  bool bugfix=true;
  std::vector<std::string> files;
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) { 
      case 'h': 
	std::cout << "Usage: " << argv[0] << " [OPTIONS]\n\n"
		  << "Options:\n"
		  << "\t-h\tThis help\n" 
		  << "\t-d\tDisable bug fix\n" 
		  << "\t-e\tEnable bug fix\n" 
		  << std::endl;
	return 0;
      case 'd': bugfix = false; break;
      case 'e': bugfix = true;  break;
      default:
	std::cerr << argv[0] << ": unknown option '" << argv[i] 
		  << "'" << std::endl;
	return 1;
      }
    }
    else 
      files.push_back(argv[i]);
  }
  
  RcuGsl::NonLinearFitter fitter;
  RcuGsl::Gaus            gaus(1,0,1);
  
  for (std::vector<std::string>::const_iterator i = files.begin(); 
       i != files.end(); i++) {
    std::ifstream in((*i).c_str());
    if (!in) { 
      std::cerr << "Failed to open " << *i << std::endl;
      continue;
    }

    std::cout << "Processing " << (*i) << " ... " << std::flush;
    RcuGsl::Histogram h(1024,-.5,1023.5);
    while (!in.eof()) {
      double x, y, ex, ey;
      in >> x >> y >> ex >> ey;
      if (in.fail()) break;
      h.Fill(x, y);
    }
    if (h.Sigma() < 0.01) { 
      std::cout << "Too small sigma" << std::endl;
      continue;
    }
    
    gaus.Set(h.MaxContent(),h.Mean(),h.Sigma());
    fitter.Set(gaus, h, bugfix);
    
    try { 
      do { 
	fitter.Step();
      } while (fitter.TestDelta(1e-3,1e-3) && fitter.Steps() < 300);
      std::cout << fitter.ChiSquare() << "/" << fitter.DegreesOfFreedom() 
		<< std::endl;
    }
    catch (std::exception& e) {
      std::cerr << e.what() << std::endl;
    }
  }
  return 0;
}

       
  
  
