// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Function.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:02:02 2007
    @brief   Base class for functions 
    
*/
#include "Function.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>

//____________________________________________________________________
RcuGsl::Function::Function(size_t np) 
  : fP(np), 
    fE(np),
    fNames(np)
{
  size_t maxn = unsigned(log10(double(np))+.5);
  for (size_t i = 0; i < np; i++) {
    std::stringstream s;
    s << "p" << std::setfill('0') << std::setw(maxn) << i;
    fNames[i] = s.str();
  }
}
//____________________________________________________________________
void 
RcuGsl::Function::Set(const std::vector<double>& p) 
{
  std::copy(p.begin(), p.end(), fP.begin());
}
//____________________________________________________________________
void 
RcuGsl::Function::Set(const std::vector<double>& p,
	      const std::vector<double>& e) 
{
  std::copy(p.begin(), p.end(), fP.begin());
  std::copy(e.begin(), e.end(), fE.begin());
}
//____________________________________________________________________
void 
RcuGsl::Function::Reset()
{
  for (size_t i = 0; i < Size(); i++) fP[i] = fE[i] = 0;
}

//____________________________________________________________________
void 
RcuGsl::Function::ToScript(std::ostream& o, Script_t t) const
{
  switch (t) {
  case kRootScript: 
    o << "  TF1* f = new TF1(\"user\",\"" << ToString(t) << "\");\n";
    for (size_t i = 0; i < Size(); i++)
      o << "  f->SetParameter(" << i << "," << fP[i]     << ");\n"
	<< "  f->SetParError("  << i << "," << fE[i]     << ");\n" 
	<< "  f->SetParName("   << i << "," << fNames[i] << ");"
	<< std::endl;
    break;
  case kGnuplotScript:
  case kRawScript:
    for (size_t i = 0; i < Size(); i++) 
      o << fNames[i] << " = " << fP[i] << ";\n";
    o << "f(x) = " << ToString(t) << ";" << std::endl;
    break;
  }
}

//____________________________________________________________________
void 
RcuGsl::Function::Print() const 
{
  std::cout << "f(x) = " << ToString(kGnuplotScript) << "\n";
  size_t maxn = 0;
  for (size_t i = 0; i < Size(); i++) 
    maxn = std::max(fNames[i].size(), maxn);
  std::cout << std::scientific;
  for (size_t i = 0; i < Size(); i++) 
    std::cout << "  "    << std::setw(maxn) << fNames[i] 
	      << " = "   << std::setw(12)   << fP[i] 
	      << " +/- " << std::setw(12)   << fE[i] << std::endl;
  std::cout << std::fixed;
}

//____________________________________________________________________
//
// EOF
//
