// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Histogram.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:02:48 2007
    @brief   Histogram class 
    
*/
#ifndef RCUGSL_Histogram
#define RCUGSL_Histogram
#ifndef RCUGSL_DataSet
# include <rcugsl/DataSet.h>
#endif
#ifndef __CCTYPE__
# include <cctype>
#endif
#ifndef __UTILITY__
# include <utility>
#endif
#ifndef __GSL_HISTOGRAM_H__
# include <gsl/gsl_histogram.h>
#endif

namespace RcuGsl
{
  
  //====================================================================
  /** @class Histogram Histogram.h <rcugsl/Histogram.h>
      @brief Structure that represents a histogram 
      @ingroup data

      For example, we can fill up a
      histogram with samples taken from a Guassian distribution 
      @code 
      // Generate Gaussian deviate 
      double RandomGaus(double mean, double sigma) 
      {
        double x, y, r2;
	do { 
          // Pick uniformly a point in the square [-1,1]x[-1,1]
          x  = 2 * double(rand()) / RAND_MAX - 1;
	  y  = 2 * double(rand()) / RAND_MAX - 1;
          r2 = x * x + y * y;
        } while (r2 > 1 || r2 == 0); // In unit circle?
	// Box-Muller transform 
	x = sigma * y * sqrt(-2 * log(r2) / r2);
	hist.Fill(x);
      }

      RcuGsl::Histogram hist(100, -1, 5);
      for (size_t i = 0; i < 1000; i++) hist.Fill(RandomGaus(2, .9));
      @endcode 
      
      If you want to normalize the histogram, you can simple scale it
      by the number entries 
      @code 
      hist *= 1/1000.;
      @endcode 

      The histogram can be plotted using GNUPlot by piping the output
      of the following into gnuplot with option @e --persist 
      @code 
      std::cout << "plot '-' using 1:2:4 with errorbars "title 'data'\n";
      h.ToScript(std::cout, RcuGsl::kGnuplotScript);
      std::cout << ";" << std::endl;
      @endcode

      Since a Histogram object is a DataSet object, a function can be
      fitted to the data using am object of one of the defined Fitter
      classes.  
  */
  struct Histogram : public DataSet
  {
    /** Range type */
    typedef std::pair<double,double> Range_t;
    /** Allocate a histogram 
	@param n Number of bins
	@param a Lower limit 
	@param b Uppper limit */
    Histogram(size_t n, double a, double b) : fH(0)  { Init(n, a, b); }
    /** Copy constructor 
	@param other Histogram to copy from. */
    Histogram(const Histogram& other);
    /** Assignment operator 
	@param other Histogram to assign from */ 
    Histogram& operator=(const Histogram& other);
    /** Deallocate a histogram */
    virtual ~Histogram();
    /** Reset the histogram. Clears all bin contents, but does not
	change the range */ 
    virtual void Reset();

    /** @{ 
	@name Axis information */
    /** Get the number of bins */
    virtual size_t Bins() const;
    /** Get the minimum X value */
    virtual double Min() const;
    /** Get the maximum X value */
    virtual double Max() const;
    /** @} */

    /** @{ 
	@name Filling */
    /** Increment bin at @a x with 1. 
	@param x Where to increment. 
	@return @c true if the histogram was modified. */
    virtual bool Fill(double x);
    /** Increment bin at @a x with the weight @a w
	@param x Where to increment.
	@param w Weight of increment. 
	@return @c true if the histogram was modified */
    virtual bool Fill(double x, double w);
    /** @} */

    /** @{ 
	@name Bin information */
    /** Get the value at bin @a i 
	@param i bin to look up. 
	@return number of counts in bin @a i 
	@throw std::domain_error if @a i is not a valid bin number */
    virtual double BinContent(size_t i) const;
    /** Get the error of bin @a i 
	@param i Bin to get the error for 
	@return  @f$ \sqrt{|x_i|}@f$ where @f$ x_i@f$ is the contents of
	bin @a i 
	@throw std::domain_error if @a i is not a valid bin number */
    virtual double BinError(size_t i) const;
    /** Get the bin range of bin @a i.  The lower bound is inclusive and
	returned in the @a first member of the returned pair.  The upper
	bound is exclusive, and returned in the @a second member of the
	returned pair. 
	@throw std::domain_error if @a i is not a valid bin number
	@param i Bin number
	@return The range @f$[x_i,x_{i+1}[@f$ */
    virtual Range_t BinRange(size_t i) const;
    /** Get the bin center of bin @a i 
	@param i The bin to query. 
	@throw std::domain_error if @a i is not a valid bin number
	@return the bin center of bin @a i */
    virtual double BinCenter(size_t i) const;
    /** Find the bin that contains @a x.  
	@param x Value to look for. 
	@return  The bin number that contains @a x, or if @a x is out of
	range, a negative number. */
    virtual int FindBin(double x) const;
    /** @} */

    /** @{ 
	@name Histogram statistics */
    /** @return Get maximum bin content. */
    virtual double MaxContent() const;
    /** @return Get the bin with the maximum bin content. */
    virtual size_t MaxBin() const;
    /** @return Get minimum bin content. */
    virtual double MinContent() const;
    /** @return Get the bin with the minimum bin content. */
    virtual size_t MinBin() const;
    /** @return The mean of the histogram */
    virtual double Mean() const;
    /** @return The sigma of the histogram */
    virtual double Sigma() const;
    /** @return The sum of the histogram */
    virtual double Sum() const;
    /** @} */
  
    /** @{ 
	@name Operations */
    /** Check if another histogram has same bins as this 
	@param other Histogram to compare to
	@return @c true if the histograms are compatible */
    bool IsCompatible(const Histogram& other) const;
    /** Add another histogram to this histogram.  Note, that the
	histograms must be compatible.  Each bin of @a other is added to
	this histogram 
	@param other histogram to add. 
	@return Reference to this histogram after the bins of @a other
	have been added.  */
    Histogram& operator+=(const Histogram& other);
    /** Substract another histogram from this histogram.  Note, that the
	histograms must be compatible.  Each bin of @a other is
	subtracted from this histogram 
	@param other histogram to substract. 
	@return Reference to this histogram after the bins of @a other
	have been subtracted.  */
    Histogram& operator-=(const Histogram& other);
    /** Multiply this histogram with another histogram.  Note, that the
	histograms must be compatible.  Each bin of this histogram is
	multiplied by the corresponding bins of @a other.
	@param other histogram to multiply with.  
	@return Reference to this histogram after multiplication with @a
	other. */
    Histogram& operator*=(const Histogram& other);
    /** Divide this histogram with another histogram.  Note, that the
	histograms must be compatible.  Each bin of this histogram is
	divided by the corresponding bins of @a other.
	@param other histogram to multiply with.  
	@return Reference to this histogram after division by @a other. */
    Histogram& operator/=(const Histogram& other);
    /** Scale each bin of this histogram with the value @a s. 
	@param s Scale factor. 
	@return Reference to this after scaling. */
    Histogram& operator*=(double s);
    /** Add the offset @a o to  each bin of this histogram.
	@param o Offset. 
	@return Reference to this after adding @a o to each bin
	content. */ 
    Histogram& operator+=(double o);
    /** @} */  

    /** @{ 
	@name Data set interface */ 
    size_t Size() const { return Bins(); }
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    double X(size_t i) const { return BinCenter(i); }
    /** @param i Entry number 
	@return  @f$ x@f$ value at point @a i */
    double Y(size_t i) const { return BinContent(i); }
    /** @param i Entry number 
	@return  The error @f$ \Delta x@f$ at point @a i */
    double Ex(size_t i) const { return (Max()-Min())/2./Bins(); }
    /** @param i Entry number 
	@return  The error @f$ \Delta y@f$ at point @a i */
    double Ey(size_t i) const { return BinError(i); }
    /** Print data set to standard out 
	@param t Script type
	@param o Output stream */
    void ToScript(std::ostream& o, Script_t t) const;
    /** @return true if we have errors */
    bool HasErrors() const { return true; }
    /** @} */
  
  protected:
    /** Initialise the histogram 
	@param n Number of bins
	@param a Lower limit
	@param b Upper limit  */
    void Init(size_t n, double a, double b);
    
    /** The histogram structure */
    gsl_histogram* fH;
  };
}

#endif
//
// EOF
//
