//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Fitter.cc
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Wed Mar 28 02:00:55 2007
    @brief   Base class for fitters 
*/
#include <rcugsl/Fitter.h>
#include <cmath>
#include <iostream>

//____________________________________________________________________
RcuGsl::Fitter::Fitter() 
  : fD(0),
    fF(0),
    fCovar(0),
    fGradient(0),
    fStep(0)
{}

//____________________________________________________________________
RcuGsl::Fitter::Fitter(Function& f, const DataSet& d) 
  : fD(0),
    fF(0),
    fCovar(0),
    fGradient(0),
    fStep(0)
{
  Set(f, d);
}

//____________________________________________________________________
RcuGsl::Fitter::~Fitter() 
{
  if (fCovar)    gsl_matrix_free(fCovar);
  if (fGradient) gsl_vector_free(fGradient);
}    

//____________________________________________________________________
void 
RcuGsl::Fitter::Set(Function& f, const DataSet& d, bool) 
{
  Function* old = fF;
  fF = &f;
  fD = &d;
  if (old && old->Size() != fF->Size()) {
    if (fCovar) gsl_matrix_free(fCovar);
    fCovar = 0;
    if (fGradient) gsl_vector_free(fGradient);
    fGradient = 0;
  }
  fP.resize(fF->Size());
  fJ.resize(fF->Size());
  // Reset to zero
  for (size_t i = 0; i < fF->Size(); i++) fP[i] = fJ[i] = 0.;
  fStep = 0;
}

//____________________________________________________________________
void 
RcuGsl::Fitter::Store() const 
{
  if (!fF) return;
  std::vector<double> p(Size());
  std::vector<double> e(Size());
  Values(p);
  Errors(e);
  fF->Set(p, e);
}

//____________________________________________________________________
void 
RcuGsl::Fitter::Print(unsigned int mask) const 
{
  if (mask != 0) return;
  std::cout << "Chi^2/NDF:\t" << ChiSquare() << "/" 
	    << DegreesOfFreedom() << " in " << Steps() << " steps"
	    << std::endl;
  if (fF) fF->Print();
}


//____________________________________________________________________
void 
RcuGsl::Fitter::Errors(Array_t& e) const 
{
  size_t   n = Size();
  Array_t  c(n * n);
  double   s = std::max(1., sqrt(ChiSquare()/DegreesOfFreedom()));
  Covariance(c);
  for (size_t i = 0; i < n; i++) e[i] = sqrt(c[i * n + i]) * s;
}

//____________________________________________________________________
size_t 
RcuGsl::Fitter::Size() const 
{ 
  return (!fF ? 0 : fF->Size());
}

//____________________________________________________________________
size_t 
RcuGsl::Fitter::Samples() const 
{ 
  return (!fD ? 0 : fD->Size());
}

//____________________________________________________________________
int
RcuGsl::Fitter::Eval(const gsl_vector* x, gsl_vector* f) 
{
  if (!fF) return GSL_EBADFUNC;
  if (!fD) return GSL_EFAULT;
  
  size_t p = Size();
  size_t n = Samples();
  for (size_t i = 0; i < p; i++) fP[i] = gsl_vector_get(x, i);
  fF->Set(fP);
  for (size_t i = 0; i < n; i++) {
    double x = fD->X(i);
    double y = fD->Y(i);
    double s = fD->HasErrors() ? fD->Ey(i) : 1;
    if (y == 0) continue;
    gsl_vector_set(f, i, (fF->operator()(x) - y) / s);
  }
  return 0;
}

//____________________________________________________________________
int
RcuGsl::Fitter::Eval(const gsl_vector* x, gsl_matrix* j) 
{
  if (!fF) return GSL_EBADFUNC;
  if (!fD) return GSL_EFAULT;
  
  size_t p = Size();
  size_t n = Samples();
  for (size_t i = 0; i < p; i++) fP[i] = gsl_vector_get(x, i);
  fF->Set(fP);
  for (size_t i = 0; i < n; i++) {
    double x = fD->X(i);
    double y = fD->Y(i);
    double s = fD->HasErrors() ? fD->Ey(i) : 1;
    if (y == 0) continue;
    fF->Jacobian(x, fJ);
    for (size_t k = 0; k < p; k++) gsl_matrix_set(j, i, k, fJ[k] / s);
  }
  return 0;
}

//____________________________________________________________________
int
RcuGsl::Fitter::Eval(const gsl_vector* x, gsl_vector* f, gsl_matrix* j) 
{
  if (!fF) return GSL_EBADFUNC;
  if (!fD) return GSL_EFAULT;
  
  size_t p = Size();
  size_t n = Samples();
  for (size_t i = 0; i < p; i++) fP[i] = gsl_vector_get(x, i);
  fF->Set(fP);
  for (size_t i = 0; i < n; i++) {
    double x = fD->X(i);
    double y = fD->Y(i);
    double s = fD->HasErrors() ? fD->Ey(i) : 1;
    if (y == 0) continue;
    gsl_vector_set(f, i, (fF->operator()(x) - y) / s);
    fF->Jacobian(x, fJ);
    for (size_t k = 0; k < p; k++) gsl_matrix_set(j, i, k, fJ[k] / s);
  }
  return 0;
}

//
// EOF
//
